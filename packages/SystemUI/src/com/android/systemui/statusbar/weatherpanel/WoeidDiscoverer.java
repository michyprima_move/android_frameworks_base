package com.android.systemui.statusbar.weatherpanel;

import android.content.Context;
import android.location.Location;
import android.location.LocationManager;
import android.util.Log;

/**
 * Created with IntelliJ IDEA.
 * User: michy_000
 * Date: 10/01/13
 * Time: 1.01
 * To change this template use File | Settings | File Templates.
 */
public class WoeidDiscoverer {

	public static String getWoeid(Location l)
	{
		//LocationManager locationManager = (LocationManager) c.getSystemService(Context.LOCATION_SERVICE);
		//Location l = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);

		String id = "12847460"; //"12847460";

		try
		{
			HttpConnectHelper h = new HttpConnectHelper();
			String raw = h.getRawString("http://query.yahooapis.com/v1/public/yql?q=select%20*%20from%20geo.placefinder%20where%20text%3D\"" + l.getLatitude() + "%2C" + l.getLongitude() + "\"%20and%20gflags%3D\"R\"");
			raw = raw.substring(raw.indexOf("<woeid>")).substring(7);
			id = raw.substring(0,raw.indexOf("<"));

		}
		catch (Exception e)
		{
			Log.i("WoeidDiscoverer", String.format("Lat: %s, Lot: %s", l.getLatitude(), l.getLongitude()));
			e.printStackTrace();
		}


		return id;
	}
}

