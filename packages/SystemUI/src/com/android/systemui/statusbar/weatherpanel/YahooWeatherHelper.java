/******************************************************************************
 * Class       : YahooWeatherHelper.java  				   		        	  *
 * Parser helper for Yahoo                                                    *
 *                                                                            *
 * Version     : v1.0                                                         *
 * Date        : May 06, 2011                                                 *
 * Copyright (c)-2011 DatNQ some right reserved                               *
 * You can distribute, modify or what ever you want but WITHOUT ANY WARRANTY  *
 * Be honest by keep credit of this file                                      *
 *                                                                            *
 * If you have any concern, feel free to contact with me via email, i will    *
 * check email in free time                                                   * 
 * Email: nguyendatnq@gmail.com                                               *
 * ---------------------------------------------------------------------------*
 * Modification Logs:                                                         *
 *   KEYCHANGE  DATE          AUTHOR   DESCRIPTION                            *
 * ---------------------------------------------------------------------------*
 *    -------   May 06, 2011  DatNQ    Create new                             *
 ******************************************************************************/
package com.android.systemui.statusbar.weatherpanel;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;

import android.util.Log;
import com.android.systemui.R;
//import com.exoplatform.weather.R;

/*******************************************************************************
 * This class for provider list country support
 * For performance issue it is better to prepare WOEID by country and city
 * 
 * Alternative for dynamic solution is use yahoo API for get country then 
 * get city then get WOEID
 * http://developer.yahoo.com/geo/geoplanet/
 * Ref: http://where.yahooapis.com/v1/countries?appid=[yourappidhere]
 * @author DatNQ
 *
 ******************************************************************************/
public class YahooWeatherHelper {
	/** For debugging */
	private static final String TAG = "YahooWeatherHelper";
	
	/** Yahoo attribute */
	private static final String PARAM_YAHOO_LOCATION = "yweather:location";
	private static final String PARAM_YAHOO_UNIT = "yweather:units";
	private static final String PARAM_YAHOO_ATMOSPHERE = "yweather:atmosphere";
	private static final String PARAM_YAHOO_CONDITION = "yweather:condition";
	private static final String PARAM_YAHOO_VALUE = "yweather:wind";
	
	/** Attribute city */
	private static final String ATT_YAHOO_CITY = "city";
	private static final String ATT_YAHOO_COUNTRY = "country";
	private static final String ATT_YAHOO_TEMP_UNIT = "temperature";
	private static final String ATT_YAHOO_HUMIDITY = "humidity";
	private static final String ATT_YAHOO_TEXT = "text";
	private static final String ATT_YAHOO_CODE = "code";
	private static final String ATT_YAHOO_DATE = "date";
	private static final String ATT_YAHOO_TEMP = "chill";
	private static final String ATT_YAHOO_VISI = "visibility";
	
	/** Image array */
	public static final int[] m_ImageArr = {
		R.drawable.a0,
		R.drawable.a2,
		R.drawable.a2,
		R.drawable.a2,
		R.drawable.a2,
		R.drawable.a5,
		R.drawable.a5,
		R.drawable.a5,
		R.drawable.a8,
		R.drawable.a9,
		R.drawable.a9,
		R.drawable.a8,
		R.drawable.a8,
		R.drawable.a13,
		R.drawable.a13,
		R.drawable.a13,
        R.drawable.a13,
		R.drawable.a19,
		R.drawable.a19,
		R.drawable.a19,
		R.drawable.a19,
		R.drawable.a19,
		R.drawable.a19,
		R.drawable.a19,
		R.drawable.a24,
		R.drawable.a25,
		R.drawable.a26,
		R.drawable.a27,
		R.drawable.a28,
		R.drawable.a29,
		R.drawable.a30,
		R.drawable.a31,
		R.drawable.a32,
		R.drawable.a33,
        R.drawable.a34,
		R.drawable.a35,
		R.drawable.a36,
		R.drawable.a2,
		R.drawable.a2,
	    R.drawable.a2,
		R.drawable.a2,
		R.drawable.a41,
		R.drawable.a41,
		R.drawable.a41,
		R.drawable.a44,
		R.drawable.a45,
		R.drawable.a46,
		R.drawable.a46,

	};

    public static final String[] weathertita = {
         "Tornado",
         "Tempesta tropicale",
            "Uragano",
            "Forte temporale",
            "Temporale",
            "Pioggia e neve" ,
             "Pioggia e nevischio",
            "Neve e grandine",
            "Pioviggine", //che ghiaccia?
            "Pioviggine",
            "Pioggia",
            "Acquazzone",
            "Acquazzone",
            "Nevischio",
            "Neve", //light snow shower
            "Neve", //blowing snow
            "Neve",
             "Grandine",
            "Nevischio",
            "Polveroni",
            "Nebbia",
             "Foschia",
             "Fumoso",
            "Burrascoso",
            "Ventoso",
            "Freddo",
            "Nuvoloso",
            "Molto nuvoloso", //notte
             "Molto nuvoloso",
            "Parz. nuvoloso", //notte
            "Parz. nuvoloso",
            "Sereno",    //notte
            "Soleggiato",
             "Sereno", //notte
            "Sereno",
            "Pioggia e grandine",
            "Caldo",
            "Temporali isolati",
            "Temporali sparsi",
            "Temporali sparsi",
            "Acquazzoni sparsi",
            "Forti nevicate",
            "Nevicate sparse",
            "Forti nevicate",
            "Parz. nuvoloso",
            "Temporale",
            "Forti nevicate",
            "Temporali isolati",
            "Non disponibile"


    };
	/***************************************************************************
	 * Parser yahoo weather
	 * @param docWeather
	 * @return
	 * @date May 9, 2011
	 * @time 2:08:58 AM
	 * @author DatNQ
	 **************************************************************************/
	public static WeatherInfo parserYahooWeatherInfo(Document docWeather){
		if (docWeather == null){
			Log.e(TAG,"Invalid doc weatehr");
			return null;
		}
		
		String strCity = null;
		String strCountry = null;
		String strTempUnit = null;
		String strTempValue = null;
		String strHumidity = null;
		String strText = null;;
		String strCode = null;;
		String strDate = null;;
		String strVisi = null;
		try {
			Element root = docWeather.getDocumentElement();
			root.normalize();
	
			NamedNodeMap locationNode = root.getElementsByTagName(PARAM_YAHOO_LOCATION).item(0).getAttributes();
			
			if (locationNode != null){
				strCity = locationNode.getNamedItem(ATT_YAHOO_CITY).getNodeValue();
				strCountry = locationNode.getNamedItem(ATT_YAHOO_COUNTRY).getNodeValue();
			}
	
			NamedNodeMap unitNode = root.getElementsByTagName(PARAM_YAHOO_UNIT).item(0).getAttributes();
			if (unitNode != null){
				strTempUnit = unitNode.getNamedItem(ATT_YAHOO_TEMP_UNIT).getNodeValue();
			}
			
			NamedNodeMap atmosNode = root.getElementsByTagName(PARAM_YAHOO_ATMOSPHERE).item(0).getAttributes();
			if (atmosNode != null){
				strHumidity = atmosNode.getNamedItem(ATT_YAHOO_HUMIDITY).getNodeValue();
				strVisi = atmosNode.getNamedItem(ATT_YAHOO_VISI).getNodeValue();
			}
			
			NamedNodeMap conditionNode = root.getElementsByTagName(PARAM_YAHOO_CONDITION).item(0).getAttributes();
			if (conditionNode != null){
				strText = "Non disponibile"; //conditionNode.getNamedItem(ATT_YAHOO_TEXT).getNodeValue();
				strCode = conditionNode.getNamedItem(ATT_YAHOO_CODE).getNodeValue();
				strDate = conditionNode.getNamedItem(ATT_YAHOO_DATE).getNodeValue();
			}
			
			NamedNodeMap temNode = root.getElementsByTagName(PARAM_YAHOO_VALUE).item(0).getAttributes();
			if (temNode != null){
				strTempValue = temNode.getNamedItem(ATT_YAHOO_TEMP).getNodeValue();
			}
		} catch (Exception e){
			Log.e(TAG,"Something wroing with parser data");
			return null;
		}

        int drawable = R.drawable.a3200;
        int nCode = Integer.parseInt(strCode);

        if (nCode < 48)
        {
           drawable = m_ImageArr[nCode];
           strText = weathertita[nCode];
        }

		/* Weather info */
		WeatherInfo yahooWeatherInfo = new WeatherInfo(strCity, strCountry, strTempValue,
				strHumidity, strText, strCode, strDate, strTempUnit, strVisi,drawable);
		
		return yahooWeatherInfo;
	}
}

