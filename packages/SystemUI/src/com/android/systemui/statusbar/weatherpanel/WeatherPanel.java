/*
 * Copyright (C) 2013 Michele Primavera
 */

package com.android.systemui.statusbar.weatherpanel;

import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Resources;
import android.database.ContentObserver;
import android.net.Uri;
import android.net.wimax.WimaxHelper;
import android.os.Handler;
import android.provider.Settings;
import android.util.AttributeSet;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.SeekBar;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.os.PowerManager;
import com.android.systemui.R;
import android.provider.Settings;
import android.provider.Settings.SettingNotFoundException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import android.os.IPowerManager;
import android.os.RemoteException;
import android.os.ServiceManager;
import android.os.Power;
import android.os.INetStatService;
import android.os.SystemProperties;
import android.telephony.TelephonyManager;
import com.android.internal.telephony.TelephonyProperties;
import android.telephony.PhoneStateListener;
import android.telephony.CellLocation;
import java.util.concurrent.TimeUnit;
import java.util.Timer;
import java.util.TimerTask;
import android.telephony.ServiceState;
import java.io.RandomAccessFile;
import java.io.File;
import java.io.IOException;
import android.net.wifi.WifiManager;
import android.net.NetworkInfo;
import android.net.ConnectivityManager;
import android.widget.ImageView;
import android.location.Location;
import android.location.LocationManager;
import com.android.systemui.statusbar.weatherpanel.WeatherInfo;
import com.android.systemui.statusbar.weatherpanel.WeatherDataModel;
import com.android.systemui.statusbar.weatherpanel.WoeidDiscoverer;
import android.location.LocationListener;
import android.os.Bundle;
import android.os.AsyncTask;
import java.lang.Void;

public class WeatherPanel extends FrameLayout {
	RelativeLayout infoLayout;
	TextView mTextView;
	ImageView mImageView;
	TextView mTextView3;
	TextView mTextView4;
	Context mContext;
	LocationManager locationManager;
	Handler mHandler;
	LocationListener locationListener;

	private static final FrameLayout.LayoutParams WIDGET_LAYOUT_PARAMS = new FrameLayout.LayoutParams(
			ViewGroup.LayoutParams.MATCH_PARENT,
			ViewGroup.LayoutParams.WRAP_CONTENT
			);

	public WeatherPanel(Context context, AttributeSet attrs) {
		super(context, attrs);
		mContext = context;
		LayoutInflater mInflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		infoLayout = (RelativeLayout)mInflater.inflate(R.layout.weatherpanel, null, false);
		mTextView = (TextView)infoLayout.findViewById(R.id.viewInfo1);
		mImageView = (ImageView)infoLayout.findViewById(R.id.viewInfo2);
		mTextView3 = (TextView)infoLayout.findViewById(R.id.viewInfo3);
		mTextView4 = (TextView)infoLayout.findViewById(R.id.viewInfo4);
		locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
		mHandler = new Handler();

		locationListener = new LocationListener() {
			public void onLocationChanged(Location location) {
				new DownloadWeatherTask().execute(location);
			}

			public void onStatusChanged(String provider, int status, Bundle extras) {}

			public void onProviderEnabled(String provider) {}

			public void onProviderDisabled(String provider) {}
		};

		mHandler.postDelayed(new Runnable() {
				public void run() {
				locationManager.requestSingleUpdate(LocationManager.NETWORK_PROVIDER, locationListener, null);
				mHandler.postDelayed(this, 3600000);
				}
				}, 60000);

		addView(infoLayout, WIDGET_LAYOUT_PARAMS);
	}

	private class DownloadWeatherTask extends AsyncTask<Location, Void, WeatherInfo> {
		protected WeatherInfo doInBackground(Location... locations) {
			return WeatherDataModel.getInstance().getWeatherData(WoeidDiscoverer.getWoeid(locations[0]));
		}

		protected void onProgressUpdate() {
		}

		protected void onPostExecute(WeatherInfo result) {
			if (result != null)
			{
				mTextView.setText(result.getCity());
				mImageView.setImageResource(result.getDrawable());
				mTextView3.setText(result.getText());
				mTextView4.setText(result.getTemperature() + "\u2103");
			}
		}
	}

}









